(let ((default-directory "~/.emacs.d/lisp/"))
  (normal-top-level-add-subdirs-to-load-path))
(require 'package)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(require 'cl)
;;(load-theme 'darcula t)
(setenv "PATH" (concat (getenv "PATH") ":~"))
(setq exec-path (append exec-path '("~")))
;;(require 'auto-complete)
;;(require 'wubi)
(add-hook 'after-init-hook #'global-flycheck-mode)
;; (register-input-method
;;  "chinese-wubi" "Chinese" 'quail-use-package "wubi" "wubi")
;; (setq default-input-method "chinese-wubi")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-PDF-mode t t)
 '(TeX-source-correlate-mode t)
 '(TeX-source-correlate-start-server t)
 '(TeX-view-program-list (quote (("Okular" "okular --unique %o#src:%n%b"))))
 '(TeX-view-program-selection
   (quote
    (((output-dvi style-pstricks)
      "dvips and gv")
     (output-dvi "Evince")
     (output-pdf "Okular")
     (output-html "xdg-open"))))
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(background-color "#202020")
 '(background-mode dark)
 '(column-number-mode t)
 '(coq-maths-menu-enable t)
 '(coq-prog-args (quote ("-I" "/home/andrei/Coq")))
 '(coq-unicode-tokens-enable t)
 '(cursor-color "#cccccc")
 '(custom-enabled-themes (quote (deeper-blue)))
 '(custom-safe-themes
   (quote
    ("41c8c11f649ba2832347fe16fe85cf66dafe5213ff4d659182e25378f9cfc183" "fad38808e844f1423c68a1888db75adf6586390f5295a03823fa1f4959046f81" "0c311fb22e6197daba9123f43da98f273d2bfaeeaeb653007ad1ee77f0003037" default)))
 '(desktop-save-mode t)
 '(display-time-mode t)
 '(ensime-sem-high-faces
   (quote
    ((var :foreground "#9876aa" :underline
          (:style wave :color "yellow"))
     (val :foreground "#9876aa")
     (varField :slant italic)
     (valField :foreground "#9876aa" :slant italic)
     (functionCall :foreground "#a9b7c6")
     (implicitConversion :underline
                         (:color "#808080"))
     (implicitParams :underline
                     (:color "#808080"))
     (operator :foreground "#cc7832")
     (param :foreground "#a9b7c6")
     (class :foreground "#4e807d")
     (trait :foreground "#4e807d" :slant italic)
     (object :foreground "#6897bb" :slant italic)
     (package :foreground "#cc7832")
     (deprecated :strike-through "#a9b7c6"))))
 '(foreground-color "#cccccc")
 '(haskell-font-lock-symbols t)
 '(haskell-indentation-electric-flag nil)
 '(haskell-indentation-layout-offset 2)
 '(haskell-indentation-left-offset 2)
 '(haskell-indentation-starter-offset 2)
 '(haskell-indentation-where-post-offset 2)
 '(haskell-indentation-where-pre-offset 2)
 '(haskell-mode-hook
   (quote
    (inf-haskell-mode interactive-haskell-mode turn-on-haskell-indentation)) t)
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-stylish-on-save t)
 '(package-selected-packages
   (quote
    (org-plus-contrib company-erlang alchemist julia-repl company company-coq erlang flycheck-tip proof-general haskell-mode ## distel-completion-lib unicode-fonts flycheck dash-functional yasnippet web tuareg shm sage-shell-mode recentf-ext rainbow-mode rainbow-identifiers rainbow-delimiters rainbow-blocks paredit octomacs merlin marmalade-demo magit kv julia-shell idris-mode hy-mode hi2 gh geiser ess-view ess-R-data-view company-math company-ghc clojure-mode-extra-font-locking biblio ac-slime)))
 '(proof-electric-terminator-enable t)
 '(proof-prog-name-ask t)
 '(proof-three-window-enable t)
 '(safe-local-variable-values
   (quote
    ((eval let
           ((default-directory
              (locate-dominating-file buffer-file-name ".dir-locals.el")))
           (make-local-variable
            (quote coq-prog-name))
           (setq coq-prog-name
                 (expand-file-name "../hoqtop")))
     (eval let
           ((unimath-topdir
             (expand-file-name
              (locate-dominating-file buffer-file-name "UniMath"))))
           (setq fill-column 100)
           (make-local-variable
            (quote coq-use-project-file))
           (setq coq-use-project-file nil)
           (make-local-variable
            (quote coq-prog-args))
           (setq coq-prog-args
                 (\`
                  ("-emacs" "-indices-matter" "-type-in-type" "-Q"
                   (\,
                    (concat unimath-topdir "UniMath"))
                   "UniMath")))
           (make-local-variable
            (quote coq-prog-name))
           (setq coq-prog-name
                 (concat unimath-topdir "sub/coq/bin/coqtop"))
           (make-local-variable
            (quote before-save-hook))
           (add-hook
            (quote before-save-hook)
            (quote delete-trailing-whitespace))
           (modify-syntax-entry 39 "w")
           (modify-syntax-entry 95 "w")
           (if
               (not
                (memq
                 (quote agda-input)
                 features))
               (load
                (concat unimath-topdir "emacs/agda/agda-input")))
           (if
               (not
                (member
                 (quote
                  ("chimney" "╝"))
                 agda-input-user-translations))
               (progn
                 (setq agda-input-user-translations
                       (cons
                        (quote
                         ("chimney" "╝"))
                        agda-input-user-translations))
                 (setq agda-input-user-translations
                       (cons
                        (quote
                         ("==>" "⟹"))
                        agda-input-user-translations))
                 (agda-input-setup)))
           (set-input-method "Agda")))))
 '(setq proof-prog-name-ask)
 '(show-paren-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Noto Mono" :foundry "GOOG" :slant normal :weight normal :height 100 :width normal))))
 '(ac-candidate-face ((t (:inherit popup-face))))
 '(rainbow-delimiters-depth-1-face ((t (:inherit rainbow-delimiters-base-face :foreground "navajo white"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "chartreuse"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "magenta"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "medium spring green"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "cornsilk"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "orange"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "SeaGreen4"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "firebrick"))))
 '(rainbow-delimiters-depth-9-face ((t (:foreground "turquoise"))))
 '(rainbow-delimiters-unmatched-face ((t (:foreground "dark red"))))
 '(unicode-tokens-symbol-font-face ((t (:slant normal :weight normal :height 128 :width normal :foundry "b&h" :family "STIXGeneral")))))

;; Set up the Common Lisp environment
(add-to-list 'load-path "~/.emacs.d/elpa/macrostep-20161120.2106")
;;(add-to-list 'load-path "~/.emacs.d/elpa/slime-20190312.2124")
(setq inferior-lisp-program "/usr/bin/clisp")
(require 'slime)
(require 'slime-autoloads)
(add-to-list 'slime-contribs 'slime-fancy)
(slime-setup)
(setq frame-title-format "emacs – %b")
(display-time)
(mouse-wheel-mode t)
 (setq auto-mode-alist (append '(("\\.cl$" . lisp-mode))
                               auto-mode-alist))
(setq slime-lisp-implementations
      '((sbcl ("sbcl" "--noinform") :coding-system utf-8-unix)
        (clisp ("clisp" "-q"))
        (cmucl ("cmucl" "-quiet"))
        ))
;;(setq slime-default-lisp 'sbcl)
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives
               '("melpa-stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives
               '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)
(require 'recentf)
(recentf-mode 1)
(global-set-key "\C-xf" 'recentf-open-files)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(setq TeX-PDF-mode t)
; comment out the following line with an initial semicolon 
; if you want to use your f12 key for something else
; or change f12 to (for example) f8
;;(global-set-key [ f12 ] 'M2)
;; Spaces only (no tab characters at all)!
(setq-default indent-tabs-mode nil)

;; Always show column numbers.
(setq-default column-number-mode t)

;; For easy window scrolling up and down.
(global-set-key "\M-n" 'scroll-up-line)
(global-set-key "\M-p" 'scroll-down-line)

;; For easier access to regex search/replace.
(defalias 'qrr 'query-replace-regexp)
(add-hook 'clojure-mode-hook 'turn-on-eldoc-mode)

;; General Auto-Complete
(require 'auto-complete-config)
(setq ac-delay 0.0)
(setq ac-quick-help-delay 0.5)
(ac-config-default)

(require 'ac-slime)
(add-hook 'slime-mode-hook 'set-up-slime-ac)
(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
(eval-after-load "auto-complete"
  '(add-to-list 'ac-modes 'slime-repl-mode))

;; ac-nrepl (Auto-complete for the nREPL)
(add-to-list 'ac-modes 'cider-mode)
(add-to-list 'ac-modes 'cider-repl-mode)
;; Poping-up contextual documentation
(eval-after-load "cider"
  '(define-key cider-mode-map (kbd "C-c C-d") 'ac-nrepl-popup-doc))
(require 'paredit)
(add-hook 'clojure-mode-hook #'enable-paredit-mode)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)

;; Show parenthesis mode
(show-paren-mode 1)


(add-to-list 'load-path "~/.emacs.d/hy-mode")
(require 'hy-mode)
(add-hook 'hy-mode-hook 'paredit-mode)

;; Haskell
(let ((my-cabal-path (expand-file-name "~/.cabal/bin")))
  (setenv "PATH" (concat my-cabal-path path-separator (getenv "PATH")))
  (add-to-list 'exec-path my-cabal-path))

(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(eval-after-load 'haskell-mode '(progn
  (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
  (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
  (define-key haskell-mode-map (kbd "C-c C-n C-t") 'haskell-process-do-type)
  (define-key haskell-mode-map (kbd "C-c C-n C-i") 'haskell-process-do-info)
  (define-key haskell-mode-map (kbd "C-c C-n C-c") 'haskell-process-cabal-build)
  (define-key haskell-mode-map (kbd "C-c C-n c") 'haskell-process-cabal)))
(eval-after-load 'haskell-cabal '(progn
  (define-key haskell-cabal-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
  (define-key haskell-cabal-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
  (define-key haskell-cabal-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
  (define-key haskell-cabal-mode-map (kbd "C-c c") 'haskell-process-cabal)))

;; Idris
(require 'idris-mode)

;;Ocaml
(require 'tuareg)
(setq auto-mode-alist 
      (append '(("\\.ml[ily]?$" . tuareg-mode))
              auto-mode-alist))
(dolist
   (var (car (read-from-string
           (shell-command-to-string "opam config env --sexp"))))
 (setenv (car var) (cadr var)))
;; Update the emacs path
(setq exec-path (split-string (getenv "PATH") path-separator))
;; Update the emacs load path
(push (concat (getenv "OCAML_TOPLEVEL_PATH")
          "/../../share/emacs/site-lisp") load-path)
;; Automatically load utop.el
(autoload 'utop "utop" "Toplevel for OCaml" t)
(autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
(add-hook 'tuareg-mode-hook 'utop-minor-mode)
(setq merlin-use-auto-complete-mode t)
(setq merlin-error-after-save nil)


;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line

;; ssreflect support
(load-file "/home/andrei/repozitorii/math-comp/mathcomp/ssreflect/pg-ssr.el")

;; Load company-coq when opening Coq files
(add-hook 'coq-mode-hook #'company-coq-mode)
(add-to-list 'auto-mode-alist '("\\.\\(sml\\|sig\\)\\'" . sml-mode))
;; rainbow delimiters
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
;;(global-rainbow-delimiters-mode)
(global-set-key [f8] 'other-frame)
(global-set-key [f7] 'paredit-mode)
(global-set-key [f9] 'cider-jack-in)
(global-set-key [f11] 'speedbar)

;; Macaulay 2 start
(load "~/.emacs-Macaulay2" t)
;; Macaulay 2 end
(add-hook 'julia-mode-hook 'julia-repl-mode)
(load-file (let ((coding-system-for-read 'utf-8))
             (shell-command-to-string "agda-mode locate")))

(set-fontset-font "fontset-default" 'unicode "Noto Mono")
(add-hook 'after-init-hook 'global-company-mode)


