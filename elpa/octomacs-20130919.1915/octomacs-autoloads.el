;;; octomacs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "octomacs" "octomacs.el" (21797 26276 354234
;;;;;;  459000))
;;; Generated autoloads from octomacs.el

(autoload 'octomacs-new-post "octomacs" "\
Create a post called POST-NAME in the Octopress work tree in DIRECTORY

\(fn POST-NAME DIRECTORY)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; octomacs-autoloads.el ends here
